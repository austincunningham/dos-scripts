rem simple program to check MX records by Austin Cunningham
echo off
rem set background colour
color 17
rem set variables
set yes=y
set yes-no=y
rem the main loop function
:checkloop
if %yes%==%yes-no% (
cls
echo ______________________________________________________________________________
echo ______________________________________________________________________________
echo.
echo  .d8888b.  888                        888           888b     d888 Y88b   d88P 
echo d88P  Y88b 888                        888           8888b   d8888  Y88b d88P  
echo 888    888 888                        888           88888b.d88888   Y88o88P   
echo 888        88888b.   .d88b.   .d8888b 888  888      888Y88888P888    Y888P    
echo 888        888 "88b d8P  Y8b d88P"    888 .88P      888 Y888P 888    d888b    
echo 888    888 888  888 88888888 888      888888K       888  Y8P  888   d88888b   
echo Y88b  d88P 888  888 Y8b.     Y88b.    888 "88b      888   "   888  d88P Y88b  
echo  "Y8888P"  888  888  "Y8888   "Y8888P 888  888      888       888 d88P   Y88b 
echo.
echo ______________________________________________________________________________
echo ______________________________________________________________________________
echo.
echo.   
goto checkmx
) 
else ( 
goto end
)
rem function to check MX to turn on other record checking remove the rem statements in checkmx
rem other record types A, ANY, CNAME, MX, NS, PTR, SOA, SRV, TXT
:checkmx
set /p domain="Domain Name: " %=%
echo.
echo record types A, ANY, CNAME, MX, NS, PTR, SOA, SRV, TXT
set /p record="Record type: " %=%
echo.
echo ================================
echo.
nslookup -q=%record% %domain%
nslookup -q=mx %domain%
echo.
echo ================================
goto checkend
rem function to check loop condition
:checkend
echo.
echo.
set /p yes-no="Search again (y/n): " %=%
goto checkloop
rem end
:end